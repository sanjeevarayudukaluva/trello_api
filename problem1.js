const https = require("https");
const { apiKey, apiToken } = require("./utils");

function getBoard(boardId) {
  const url = `https://api.trello.com/1/boards/${boardId}/memberships?key=${apiKey}&token=${apiToken}`;

  return new Promise((resolve, reject) => {
    const request = https.get(url, (res) => {
      let data = "";

      res.on("data", (chunk) => {
        data += chunk;
      });

      res.on("end", () => {
        if (res.statusCode !== 200) {
          reject(`Failed to fetch board. Status code: ${res.statusCode}`);
          return;
        }

        try {
          resolve(JSON.parse(data));
        } catch (error) {
          reject(error);
        }
      });
    });

    request.on("error", (error) => {
      reject(error);
    });
  });
}

module.exports = getBoard;
