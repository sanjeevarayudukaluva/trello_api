const https = require("https");
const { apiKey, apiToken } = require("./utils");

function getCards(listId) {
  const url = `https://api.trello.com/1/lists/${listId}/cards?key=${apiKey}&token=${apiToken}`;
  return new Promise((resolve, reject) => {
    const request = https.get(url, (res) => {
      let data = "";

      res.on("data", (chunks) => {
        data += chunks;
      });

      res.on("end", () => {
        try {
          if (res.statusCode !== 200) {
            reject(`Failed to fetch cards. Status code: ${res.statusCode}`);
            return;
          }
          resolve(JSON.parse(data));
        } catch (error) {
          reject(error);
        }
      });
    });

    request.on("error", (error) => {
      reject(error);
    });
  });
}

module.exports = getCards;
