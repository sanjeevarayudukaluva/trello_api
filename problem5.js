const getCards = require("./problem4.js");
const createList = require("./problem3.js");

function getAllCards(boardId) {
  return new Promise((resolve, reject) => {
    createList(boardId)
      .then((lists) => {
        const promiseList = lists.map((list) => getCards(list.id));
        return Promise.all(promiseList);
      })
      .then((res) => {
        resolve(res);
      })
      .catch((err) => {
        reject(err);
      });
  });
}

module.exports = getAllCards;
