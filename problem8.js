const getLists = require("./problem3.js");
const deleteLists = require("./problem7.js");

const boardId = "ALYIgLxw";

getLists(boardId)
  .then((listsData) => {
    let lists = listsData.map((list) => list.id);
    let result = lists.reduce((prevPromise, currentlistId) => {
      return prevPromise.then((res) => {
        console.log(res);
        return deleteLists(currentlistId).catch((error) => {
          console.error(`Failed to delete list ${currentlistId}: ${error}`);
          return res; 
        });
      });
    }, Promise.resolve());

    return result;
  })
  .then((res) => {
    console.log(res);
  })
  .catch((error) => {
    console.log(error);
  });
