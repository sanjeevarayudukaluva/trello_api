let https = require("https");

let { apiToken, apiKey } = require("./utils");

const options = {
  method: "PUT",
};

function deleteLists(id) {
  let urlPath = `https://api.trello.com/1/lists/${id}/closed?key=${apiKey}&token=${apiToken}&value=true`;

  return new Promise((resolve, reject) => {
    let request = https.request(urlPath, options, (res) => {
      let data = "";
      res.on("data", (chunck) => {
        data = data + chunck;
      });
      res.on("end", () => {
        if (res.statusCode !== 200) {
          reject(`Failed to delete list. Status code: ${res.statusCode}`);
          return;
        }
        try {
          resolve(JSON.parse(data));
        } catch (error) {
          reject(error);
        }
      });
      res.on("error", (error) => {
        reject(error);
      });
    });
    request.end();
  });
}

module.exports = deleteLists;
