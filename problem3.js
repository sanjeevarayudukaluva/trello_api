const https = require("https");
const { apiKey, apiToken } = require("./utils");

function createList(boardId) {
  const url = `https://api.trello.com/1/boards/${boardId}/lists?key=${apiKey}&token=${apiToken}`;
  return new Promise((resolve, reject) => {
    const request = https.get(url, (res) => {
      let data = "";

      res.on("data", (chunks) => {
        data += chunks;
      });

      res.on("end", () => {
        try {
          if (res.statusCode !== 200) {
            reject(`Failed to fetch lists. Status code: ${res.statusCode}`);
            return;
          }
          resolve(JSON.parse(data));
        } catch (error) {
          reject(error);
        }
      });
    });

    request.on("error", (error) => {
      reject(error);
    });
  });
}

module.exports = createList;
