const { apiKey, apiToken } = require("./utils");

function setStateOfCheckItem(cardId, checkItemId) {
  let urlPath = `https://api.trello.com/1/cards/${cardId}/checkItem/${checkItemId}?key=${apiKey}&token=${apiToken}&state=incomplete`;
 return new Promise((resolve,reject)=>{
  setTimeout(() => {
    fetch(urlPath, {
     method: "PUT",
   })
     .then((response) => {
       if (!response.ok) {
         throw new Error(
           `Failed to set state of check item: ${response.status} ${response.statusText}`
         );
       }
       return response.json();
     })
     .catch((error) => {
       console.error("Error setting state of check item:", error);
       throw error;
     });
 }, 2000);
 })
}
module.exports = setStateOfCheckItem;
