let https = require("https");

let { apiToken, apiKey } = require("./utils");

let post = {
  method: "POST",
  headers: {
    "Content-Type": "/json",
  },
};

function createList(name, boardId) {
  let urlPath = `https://api.trello.com/1/lists?name=${name}&idBoard=${boardId}&key=${apiKey}&token=${apiToken}`;
  return new Promise((resolve, reject) => {
    const request = https.request(urlPath, post, (res) => {
      let data = "";
      res.on("data", (chunck) => {
        data = data + chunck;
      });
      res.on("end", () => {
        if (res.statusCode !== 200) {
          reject(`Failed to create list. Status code: ${res.statusCode}`);
          return;
        }
        try {
          resolve(JSON.parse(data));
        } catch (error) {
          reject(error);
        }
      });
      res.on("error", (error) => {
        reject(error);
      });
    });
    request.end();
  });
}

function createCard(name, listId) {
  let urlPath = `https://api.trello.com/1/cards?name=${name}&idList=${listId}&key=${apiKey}&token=${apiToken}`;
  return new Promise((resolve, reject) => {
    const request = https.request(urlPath, post, (res) => {
      let data = "";
      res.on("data", (chunck) => {
        data = data + chunck;
      });
      res.on("end", () => {
        if (res.statusCode !== 200) {
          reject(`Failed to create card. Status code: ${res.statusCode}`);
          return;
        }
        try {
          resolve(JSON.parse(data));
        } catch (error) {
          reject(error);
        }
      });
      res.on("error", (error) => {
        reject(error);
      });
    });
    request.end();
  });
}

module.exports = { createList, createCard };
