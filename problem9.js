const { apiKey, apiToken } = require("./utils");

function setStateOfCheckItem(cardId, checkItemId) {
  let urlPath = `https://api.trello.com/1/cards/${cardId}/checkItem/${checkItemId}?key=${apiKey}&token=${apiToken}&state=complete`;
  return fetch(urlPath, {
    method: "PUT",
  })
    .then((response) => {
      if (!response.ok) {
        throw new Error(
          `Failed to set state of check item: ${response.status} ${response.statusText}`
        );
      }
      return response.json();
    })
    .catch((error) => {
      console.error("Error setting state of check item:", error);
      throw error;
    });
}

function getCheckItems(checkListID) {
  let urlPath = `https://api.trello.com/1/checklists/${checkListID}/checkItems?key=${apiKey}&token=${apiToken}`;
  return fetch(urlPath, {
    method: "GET",
  })
    .then((response) => {
      if (!response.ok) {
        throw new Error(
          `Failed to fetch check items: ${response.status} ${response.statusText}`
        );
      }
      return response.json();
    })
    .catch((error) => {
      console.error("Error fetching check items:", error);
      throw error;
    });
}

module.exports = { setStateOfCheckItem, getCheckItems };
