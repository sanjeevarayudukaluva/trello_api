const https = require("https");
const { apiKey, apiToken } = require("./utils");

function createBoard(name) {
  return new Promise((resolve, reject) => {
    const postData = JSON.stringify({ name });
    const url = `https://api.trello.com/1/boards/?name=${name}&key=${apiKey}&token=${apiToken}`;

    const options = {
      method: "POST",
      headers: {
        "Content-Type": "text/json",
      },
    };

    const req = https.request(url, options, (res) => {
      let data = "";

      res.on("data", (chunk) => {
        data += chunk;
      });

      res.on("end", () => {
        if (res.statusCode !== 200) {
          reject(`Failed to create board. Status code: ${res.statusCode}`);
          return;
        }

        try {
          resolve(JSON.parse(data));
        } catch (error) {
          reject(error);
        }
      });
    });

    req.on("error", (error) => {
      reject(error);
    });

    req.end(postData);
  });
}

module.exports = createBoard;
