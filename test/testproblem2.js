const createBoard = require("../problem2.js");

try {
  createBoard("mb4")
    .then((response) => {
      console.log("Board created  ", response);
    })
    .catch((error) => {
      console.error(`Error occured while creating board ${error}`);
    });
} catch (error) {
  console.error(error);
}
