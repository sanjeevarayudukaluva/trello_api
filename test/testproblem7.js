let createList = require("../problem3");

let deleteLists = require("../problem7");

createList("Z69xUxos").then((res) => {
  console.log(res);
  let data = res.map((ele) => deleteLists(ele.id));
  return Promise.all(data);
});
