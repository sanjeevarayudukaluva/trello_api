const getAllCards = require("../problem5.js");
const { getCheckItems } = require("../problem9.js");
const setStateOfCheckItem = require("../problem10.js");

let object = {};
getAllCards("xAMiCigc")
  .then((cardsData) => {
    console.log(cardsData);
    cardsData = cardsData.flat();
    console.log(cardsData);
    let promiseArr = [];
    cardsData.forEach((card) => {
      object[card.id] = card.idChecklists;
      card.idChecklists.forEach((listId) => {
        promiseArr.push(getCheckItems(listId));
      });
    });
    return Promise.all(promiseArr);
  })
  .then((res) => {
    // console.log(res);
    res = res.flat();
    console.log(res);
    let promiseArr = res.map((checkItems) => {
      let card = Object.keys(object).find((id) => {
        return object[id].includes(checkItems.idChecklist);
      });
      return setStateOfCheckItem(card, checkItems.id);
    });
    return Promise.all(promiseArr);
  })
  .then((res) => console.log(res))
  .catch((err) => console.error(err));
