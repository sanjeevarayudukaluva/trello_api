let createBoard = require("../problem2.js");
let { createList, createCard } = require("../problem6.js");

createBoard("board1")
  .then((res) => {
    let result = [];
    for (let i = 0; i < 3; i++) {
      result.push(createList(`list-${i}`, res.id));
    }
    return Promise.all(result);
  })
  .then((res) => {
    let result = [];
    for (let i = 0; i < 3; i++) {
      result.push(createCard(`card1-${i}`, res[i].id));
    }
    return Promise.all(result);
  })
  .then((res) => {
    console.log(res);
  })
  .catch((error) => {
    console.log(error);
  });
